ESRBBRT README
====================
ESRBBRT.lvproj is used to develop a realtime application for barrier-bucket cavity control based on NI LabVIEW Real-Time Control (NI-DAQmx) template.
Currently used development SW is LabVIEW 2021.

Related documents and information
=================================-
- README.md
- Release_Notes.md
- EUPL v.1.1 - Lizenz.pdf & EUPL v.1.1 - Lizenz.rtf
- Contact: your email
- Download, bug reports... : Git Repository URL
- Documentation:
  - Refer to virtual folder ´Project Documentation´ in ESRBBRT.lvproj
  - https://www.gsi.de/work/gesamtprojektleitung_fair/sis100sis18_sis/ring_rf_systems_rrf/aktivitaeten?no_cache=1
  - https://www.gsi.de/work/gesamtprojektleitung_fair/sis100sis18_sis/ring_rf_systems_rrf/ring_rf_cavity_technology_rct
  - https://www.gsi.de/work/gesamtprojektleitung_fair/sis100sis18_sis/ring_rf_systems_rrf/veroeffentlichungen?no_cache=1

GSI - Arbeitsnotiz
------------------
Nr.: 20181122
Zielformulierung des Teilprojekts Controller-software für ESR-BB
Name: M. Frey
Nach Projektende soll ein Controller von National Instruments die Anlagensteuerung fur beide Kavitäten mittels eines Labview-Programms ("VI") übernehmen.
Diese Software soll möglichst hohe Ausfallsicherheit und damit hohe Anlagenverfügbarkeit sicherstellen.
Die Software soll mit der bereits vorhandenen Hardware getestet sein und kompatibel mit der SPS-Anlagensteuerung sowie dem Beschleuniger-Kontrollsystem sein.


Known issues:
=============
   - Development in progress

Author: H.Brand@gsi.de

Copyright 2021  GSI Helmholtzzentrum für Schwerionenforschung GmbH

EEL, Planckstr.1, 64291 Darmstadt, Germany

Lizenziert unter der EUPL, Version 1.1 oder - sobald diese von der Europäischen Kommission genehmigt wurden - Folgeversionen der EUPL ("Lizenz"); Sie dürfen dieses Werk ausschließlich gemäß dieser Lizenz nutzen.

Eine Kopie der Lizenz finden Sie hier: http://www.osor.eu/eupl

Sofern nicht durch anwendbare Rechtsvorschriften gefordert oder in schriftlicher Form vereinbart, wird die unter der Lizenz verbreitete Software "so wie sie ist", OHNE JEGLICHE GEWÄHRLEISTUNG ODER BEDINGUNGEN - ausdrücklich oder stillschweigend - verbreitet.

Die sprachspezifischen Genehmigungen und Beschränkungen unter der Lizenz sind dem Lizenztext zu entnehmen