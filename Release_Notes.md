Release Notes for the ESRBB Project
===========================================
_ESRBBRT.lvproj_ is used to develop a realtime application for barrier-bucket cavity control based on NI LabVIEW Real-Time Control (NI-DAQmx) template.
Currently used development SW is LabVIEW 2021.


Version 0.0.0.0 23-09-2021 H.Brand@gsi.de
--------------------------------------
The ESRBB project was just created.

